# Copyright 2013 VMware, Inc.  All rights reserved. -- VMware Confidential
#

import subprocess import Popen, PIPE
import re
import time

def main():
    # 0. Deploy secondary appliance
    p = Popen("/home/deliverable/deploy_appliance.sh",
              stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    # Get secondary appliance IP    
    print "STDOUT:%s\nSTDERR:%s",(stdout, stderr)
    vm_ip = re.findall(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', stdout)[3]
    print vm_ip
    # 10 MINUTES
    time.sleep(600)
#     vm_ip = "10.26.226.181"
    commands = [# 1. Stop all services on secondary appliance
               "sshpass -p 'vmware' ssh -o StrictHostKeyChecking=no root@%s \"service-control --stop --all\"" % vm_ip,
               # 2. export SSO service data
               "sh /usr/lib/vmidentity/tools/scripts/sso2_export.sh",
               # 3. Transfers export data to secondary appliance 
               "sshpass -p 'vmware' scp /usr/lib/vmidentity/tools/scripts/exported_sso.properties root@%s:/usr/lib/vmidentity/tools/scripts/exported_sso.properties"%vm_ip,
               # 4. Import data to secondary appliance
               "sshpass -p 'vmware' ssh -o StrictHostKeyChecking=no root@%s \"/usr/lib/vmidentity/tools/scripts/sso_import.sh\"" % vm_ip,
               # 5. Start all services on secondary appliance
               "sshpass -p 'vmware' ssh -o StrictHostKeyChecking=no root@%s \"service-control --start --all\"" % vm_ip,
               # 6. repoint primary VC to SSO on secondary appliance
               "/usr/lib/vmware-vmafd/bin/vmafd-cli set-dc-name --server-name localhost --dc-name %s" % vm_ip,
               # 7. stop all services on primary appliance
               "service-control --stop --all",
               # 8. start all services on primary appliance (except SSO)
               "service-control --start --all"]
    for cmd in commands:
        print "command:%s", cmd
        p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        with open('output.txt', 'a') as fp:
            fp.write("Command:%s \nSTDOUT:%s\nSTDERR:%s\n"%(cmd, stdout, stderr))
        print 'Executed command %s --stdout: %s, --stderr: %s', (cmd, stdout, stderr)

if __name__ == '__main__':
    main()

