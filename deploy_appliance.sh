#!/bin/sh

# Custom configuration

container_ip="10.26.226.30"

SBNUM=3900390
URL_TO_CLOUDVM_OVF="http://build-squid.eng.vmware.com/build/mts/release/bora-2445692/publish/VMware-vCenter-Server-Appliance-6.0.0.5100-2445692_OVF10.ovf"
CLOUDVM_VMNAME="secondary_node_vm_4"
DESTINATION_ESX_DATASTORE="datastore1 (1)"
DESTINATION_ESX_NETWORK="VM Network"
DEST_ESX_USER="root"
DEST_ESX_PASS="ca\$hc0w"
DEST_ESX_IP=${container_ip}

 #--prop:guestinfo.cis.system.vm0.hostname="${INFRA_NODE_IP}" \
#./ovftool/ovftool
/home/asparuhov/ovftool/ovftool \
 --powerOn --acceptAllEulas --X:waitForIp --X:injectOvfEnv \
 --prop:guestinfo.cis.appliance.ssh.enabled=True \
 --prop:guestinfo.cis.appliance.root.shell="/bin/bash" \
 --prop:guestinfo.cis.appliance.root.passwd=vmware \
 --prop:guestinfo.cis.vmdir.password=vmware \
 --prop:guestinfo.cis.vmdir.domain-name=vsphere.local \
 --prop:guestinfo.cis.deployment.node.type="embedded" \
 --name="${CLOUDVM_VMNAME}" \
 --datastore="${DESTINATION_ESX_DATASTORE}" \
 --network="${DESTINATION_ESX_NETWORK}" \
 "${URL_TO_CLOUDVM_OVF}" \
 "vi://${DEST_ESX_USER}:${DEST_ESX_PASS}@${DEST_ESX_IP}" 

